<?php 
	class Answers {
		public $lead_id;
		public $user_id;
		public $producto;
		public $precio;
		public $cliente;
		public $question1;
		public $question2;
		public $question3;
		public $question4;
		public $question5;
		private $host = "localhost";
		private $user = "survey";
		private $password = "V88Tig1";
		private $database = "script_db";
		
		function __construct($lead_id, $user_id, $producto, $precio, $cliente, $question1, $question2, $question3, $question4, $question5) {
			$this->lead_id   = $lead_id;
			$this->user_id   = $user_id;
			$this->producto  = $producto;
			$this->precio  = $precio;
			$this->cliente   = $cliente;
			$this->question1 = $question1;
			$this->question2 = $question2;
			$this->question3 = $question3;
			$this->question4 = $question4;
			$this->question5 = $question5;
		}

		public function save() {
			$cn = new mysqli($this->host, $this->user, $this->password, $this->database);

			if ($cn->connect_errno){
				$result = [ 'status' => 400, 'message' => "Error al conectarse a la base de datos" ];
				return $result;
			} 

			if($query = $cn->query("insert into asegurados (lead_id, user_id, producto, precio, cliente, question1, question2, question3, question4, question5, created, modified) values ('$this->lead_id', '$this->user_id', '$this->producto', $this->precio, '$this->cliente', '$this->question1', '$this->question2', '$this->question3', '$this->question4', '$this->question5', NOW(), NOW())")){
				$result = [ 'status' => 200, 'message' => "Respuestas Guardadas" ];
				return $result;
			}else{
				$result = [ 'status' => 400, 'message' => "Error al guardar " .$cn->error ];
				return $result;
			}

			mysqli_close($cn);
		}
	}

	$answer = new Answers($_POST['lead_id'], $_POST['user_id'], $_POST['producto'], $_POST['precio'], $_POST['cliente'], $_POST['question1'], $_POST['question2'], $_POST['question3'], $_POST['question4'], $_POST['question5']);
	echo json_encode( $answer->save())
?>
