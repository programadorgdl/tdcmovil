<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Reporte</title>
	<!-- Custom CSS -->
		<link rel="stylesheet" href="css/style.css">

		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="css/bootstrap.min.css">

		<!-- Optional theme -->
		<link rel="stylesheet" href="css/bootstrap-theme.min.css">

		<!-- Latest compiled and minified JavaScript -->
		<script src="js/jquery-3.1.1.min.js"></script>
                <script src="js/bootstrap.min.js"></script>

	<script src="js/filesaver.js"></script>
	<script src="js/tableexport.js"></script>
</head>
<body>
    <h2 class="section-header">Crear reporte de encuestas </h2>
    <div class="container-fluid box">            
	    <form name ="form1" method ="post" action ="report.php">        
	        <div class="col-md-12">
	            <div class="col-xs-3">          
	                <div class="form-group">
	                <label for="usr">Fecha inicio:</label>
	                <input type="date" class="form-control" id="dateInicio" name="initial_date">
	              </div>  
	            </div>
	             <div class="col-xs-3">  
	                <div class="form-group">
	                <label for="usr">Fecha final:</label>
	                <input type="date" class="form-control" id="dateFinal" name="final_date">
	              </div>   
	            </div>
	           	<div class="col-xs-3">  
	                <div class="form-group">
	                	<label for="usr">Selecciona reporte:</label><br>
	                	<select name="sitios" id="sitio" class="form-control">
		                	<option value="">Seleccione...</option>
		                	<option value="Cliente TDCMOVIL">Cliente TDCMOVIL</option>
							<option value="Asegurados TDCMOVIL">Asegurados TDCMOVIL</option>
						</select>
	              	</div>   
	            </div>
	            <div class="col-xs-3">
	              <Input type = "submit" class="btn btn-info bottom-survey" Name = "" id = "survey-date" VALUE = "Enviar" >
	            </div>
	       	</div>
	    </form>
	    <?php if (isset($data)):?>
			<table class="table table-striped" id="table">
	        <thead>
	           <tr>
	            <th>Id</th>
	            <th>lead id</th>
	            <th>User id</th>
	            <th>Producto</th>
	            <th>Precio</th>
	            <th>Cliente</th>
	            <th>Question 1</th>
	            <th>Question 2</th>
	            <th>Question 3</th>
	            <th>Question 4</th>
	            <th>Question 5</th>
	            <th>Question 6</th>
	            <th>Question 7</th>
	            <th>Question 8</th>
	            <th>Created </th>
	        </tr> 
	        </thead>
	        <body>
	    	<?php foreach($data as $row): ?>
                <tr>
	                <td> <?php echo$row->id;?></td>
	                <td> <?php echo$row->lead_id ?> </td>
	                <td> <?php echo$row->user_id ?> </td>
	                <td> <?php echo$row->producto ?> </td>
	                <td> <?php echo$row->precio ?> </td>
	                <td> <?php echo$row->cliente ?> </td>
	                <td> <?php echo$row->question1?> </td>
	                <td> <?php echo$row->question2?> </td>
	                <td> <?php echo$row->question3?> </td>
	                <td> <?php echo$row->question4?> </td>
	                <td> <?php echo$row->question5?> </td>
	                <td> <?php echo$row->question6?> </td>
	                <td> <?php echo$row->question7?> </td>
	                <td> <?php echo$row->question8?> </td>
	                <td> <?php echo$row->created?> </td>
                </tr>
	    	<?php endforeach ?>
	    	</body>                    
	        </table>
		<?php endif?>

    </div> 
    
</body>
</html>

<script>
	$("#table").tableExport({
    	bootstrap: true,
    	fileName:'Report survey'
});

function abreSitio(){
	var URL = "http://localhost/";
	var web = document.form1.sitio.options[document.form1.sitio.selectedIndex].value;
	window.open(URL+web);
}
</script>
