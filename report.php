<?php 

/**
* 
*/
class Report 
{
	private $host = "localhost";
	private $user = "survey";
	private $password = "V88Tig1";
	private $database = "script_db";
	private $tables = [
		'Cliente TDCMOVIL' => "clientes", 
		'Asegurados TDCMOVIL' => "asegurados"
	];
	private $connection;

	public function __construct()
	{
		$this->connection = new mysqli(
			$this->host,
			$this->user,
			$this->password,
			$this->database
		);

		if($this->connection->connect_error)
			die('Error de Conexión (' . $this->connection->connect_errno . ') '. $this->connection->connect_error);		
	}

	public function survery($initial_date, $final_date, $key)
	{
		 
		$table = $this->getTable($key);
		$queryWhere = $this->where($table ,[
			[
				'DATE(created)',
				'>=',
				"'$initial_date'"
			],
			[
				'DATE(created)',
				'<=',
				"'$final_date'"
			]
		]);
		

		if($query = $this->connection->query($queryWhere)){

			foreach ($query as $row) {
				$data[] = (object) ['id' => $row['id'], 'lead_id' => $row['lead_id'], 'user_id' => $row['user_id'],'producto' => $row['producto'],'precio' => $row['precio'],'cliente' => $row['cliente'],'question1' => $row['question1'], 'question2' => $row['question2'], 'question3' => $row['question3'],'question4' => $row['question4'],'question5' => $row['question5'],'question6' => $row['question6'],'question7' => $row['question7'],'question8' => $row['question8'], 'created' => $row['created']];
			}

    		require_once 'layout.php';

    	}else{
    		return false;
    	}

    	$this->connection->close();		 
		
	}

	private function getTable($key){

		if (array_key_exists($key, $this->tables)) {
		   return $this->tables[$key];
		}
		
	}


	public function select($table = null)
	{
		return "SELECT * FROM $table";
	}

	public function where($table ,$options = [])
	{
		$query = " WHERE ";

		$last_option = count($options) - 1;

		foreach ($options as $key => $option) {
			$query = $query . "$option[0] ";
			$query = $query . "$option[1] ";
			$query = $query . "$option[2] ";

			if(count($option) > 1 && $last_option != $key)
				$query = $query . "AND ";
		}

		$querySelect = $this->select($table);

		return $querySelect . $query;

	}
}


if( (isset($_POST['initial_date']) && isset($_POST['final_date'])) && 
	(!empty($_POST['initial_date'])  && !empty($_POST['final_date']))){

	$report = new Report();
	$report->survery($_POST['initial_date'],$_POST['final_date'], $_POST['sitios']);

}


require_once('layout.php');

?>
