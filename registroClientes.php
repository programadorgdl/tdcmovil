<?php 
	class Answers {
		public $lead_id;
		public $user_id;
		public $producto;
		public $precio;
		public $question1;
		public $question2;
		public $question3;
		public $question4;
		public $question5;
		public $question6;
		public $question7;
		public $question8;
		private $host = "localhost";
		private $user = "survey";
		private $password = "V88Tig1";
		private $database = "script_db";
		
		function __construct($lead_id, $user_id, $producto, $precio, $question1, $question2, $question3, $question4, $question5, $question6, $question7, $question8) {
			$this->lead_id   = $lead_id;
			$this->user_id   = $user_id;
			$this->producto  = $producto;
			$this->precio  = $precio;
			$this->question1 = $question1;
			$this->question2 = $question2;
			$this->question3 = $question3;
			$this->question4 = $question4;
			$this->question5 = $question5;
			$this->question6 = $question6;
			$this->question7 = $question7;
			$this->question8 = $question8;
		}

		public function save() {
			$cn = new mysqli($this->host, $this->user, $this->password, $this->database);

			if ($cn->connect_errno){
				$result = [ 'status' => 400, 'message' => "Error al conectarse a la base de datos" ];
				return $result;
			} 

			if($query = $cn->query("insert into clientes (lead_id, user_id, producto, precio, question1, question2, question3, question4, question5, question6, question7, question8, created, modified) values ('$this->lead_id', '$this->user_id', '$this->producto', '$this->precio', '$this->question1', '$this->question2', '$this->question3', '$this->question4', '$this->question5', '$this->question6', '$this->question7', '$this->question8', NOW(), NOW())")) {
				$result = [ 'status' => 200, 'message' => "Respuestas Guardadas" ];
				return $result;
			} else {
				$result = [ 'status' => 400, 'message' => "Error al guardar " .$cn->error ];
				return $result;
			}

			mysqli_close($cn);
		}
	}

	$answer = new Answers($_POST['lead_id'], $_POST['user_id'], $_POST['producto'], $_POST['precio'], $_POST['question1'], $_POST['question2'], $_POST['question3'], $_POST['question4'], $_POST['question5'], $_POST['question6'], $_POST['question7'], $_POST['question8']);
	echo json_encode( $answer->save())
?>
